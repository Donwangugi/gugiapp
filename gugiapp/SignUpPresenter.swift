//
//  SignUpPresenter.swift
//  gugiapp
//
//  Created by Aaron Wangugi on 7/9/15.
//  Copyright © 2015 Aaron Wangugi. All rights reserved.
//

import Foundation
import UIKit

class SignUpPreseneter: SignUpPresenterProtocol, SignUpInteractorOutputProtocol
{
	weak var view: SignUpViewProtocol?
	weak var userNameView: UsernameViewProtocol?
	var interactor: SignUpInteractorInputProtocol?
	var wireFrame: SignUpWireFrameProtocol?
	
	init() {}
	
	func userNameViewDidLoad() {
		self.userNameView?.setNextButtonTitle(NSLocalizedString("Sign Up", comment: "comment")) //TODO: Investigate what the comment is
		self.userNameView?.setLogo(UIImage(named: "gugi_logo")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)) //TODO: Investigate what rendering mode means
		
	}
	
	func viewDidLoad() {
		
	}
	
	func userDidSelectNext() {
		self.interactor?.checkUserName({ [weak self] (error: String?) -> () in
			if let err = error {
				self?.userNameView?.showError(err)
			}
			else {
				self?.wireFrame?.presentHome(fromView: self!.view!, completion: nil)
			}
			}, username: self.userNameView?.getUsername())
	}
	
	func userDidSelectSignUp() {
		self.interactor?.signUp({ [weak self] (error: String?) -> () in
			if let err = error {
				self?.view?.showError(err)
			}
			else {
				self?.wireFrame?.presentHome(fromView: self!.view!, completion: nil)
			}
		}, signUpItem: nil)
	}
	
}