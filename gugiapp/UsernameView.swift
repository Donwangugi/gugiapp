//
//  UsernameView.swift
//  gugiapp
//
//  Created by Aaron Wangugi on 7/6/15.
//  Copyright © 2015 Aaron Wangugi. All rights reserved.
//

import Foundation
import UIKit
import PureLayout_iOS

class UsernameView: UIViewController, UsernameViewProtocol
{
	// MARK: Styles
	private struct Styles {
		static private let BUTTON_HEIGHT: CGFloat = 50
		static private let BUTTON_COLOR: UIColor = Stylesheet.COLOR_BLUE_LIGHT
		static private let FLOATING_CORNER_RADIUS: CGFloat = 10
		static private let FLOATING_HEIGHT_PERCENTAGE: CGFloat = 0.3
		static private let FLOATING_WIDTH_PERCENTAGE: CGFloat = 0.8
		static private let LOGO_SIZE: CGFloat = 80
		static private let MARGIN_20: CGFloat = 20
	}
	
	// MARK: Attributes
	
	var presenter: SignUpPresenterProtocol?
	lazy var logoImageView: UIImageView = UIImageView()
	lazy var nextButton: UIButton = {
		var button = UIButton()
		button.backgroundColor = Styles.BUTTON_COLOR
		return button
	}()
	lazy var userNameTextField: UITextField = {
		var textField = UITextField()
		textField.backgroundColor = Styles.BUTTON_COLOR
		textField.text = "Username"
		textField.borderStyle = UITextBorderStyle.RoundedRect
		return textField
	}()
	
	// MARK: - View Life Cycle
	
	override func viewDidLoad() {
		self.setupSubviews()
		self.setupConstraints()
		//TODO: self.setNeedsStatusBarAppearanceUpdate()
		self.presenter?.viewDidLoad()
	}
	
	override func viewDidDisappear(animated: Bool) {
		super.viewDidDisappear(animated)
	}
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
	}
	
	// MARK: - Subviews
	
	
	/**
	Setup subviews and add them ot the main view
	*/
	private func setupSubviews()
	{
		self.view.addSubview(self.nextButton)
		self.view.addSubview(self.userNameTextField)
		self.nextButton.addTarget(self, action: Selector("userDidSelectNext:"), forControlEvents: UIControlEvents.TouchUpInside)
		self.view.addSubview(self.logoImageView)
	}
	
	private func setupConstraints()
	{
		self.nextButton.translatesAutoresizingMaskIntoConstraints = false
		self.nextButton.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: self.view, withMultiplier: 0.8)
		self.nextButton.autoSetDimension(ALDimension.Height, toSize: Styles.BUTTON_HEIGHT)
		self.nextButton.autoCenterInSuperview()
		
		self.userNameTextField.translatesAutoresizingMaskIntoConstraints = false
		self.userNameTextField.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: self.view, withMultiplier: 0.8)
		self.userNameTextField.autoSetDimension(ALDimension.Height, toSize: Styles.BUTTON_HEIGHT)
		self.userNameTextField.autoPinEdge(ALEdge.Bottom, toEdge: ALEdge.Top, ofView: self.nextButton, withOffset: -30)
		self.userNameTextField.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self.nextButton)
		
		self.logoImageView.translatesAutoresizingMaskIntoConstraints = false
		self.logoImageView.autoSetDimension(ALDimension.Height, toSize: Styles.LOGO_SIZE)
		self.logoImageView.autoSetDimension(ALDimension.Width, toSize: Styles.LOGO_SIZE)
		self.logoImageView.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self.nextButton)
		self.logoImageView.autoPinEdge(ALEdge.Bottom, toEdge: ALEdge.Top, ofView: self.nextButton, withOffset: -Styles.MARGIN_20)
	}

    //MARK: UsernameViewProtocol

    func setNextButtonTitle(let title: String)
    {
        self.nextButton.setTitle(title, forState: UIControlState.Normal)
    }

    func setLogo(let image: UIImage)
    {
        self.logoImageView.image = image
        self.logoImageView.tintColor = UIColor.purpleColor()
    }

    func showError(let errorMessage: String)
    {
        //turn this into
		self.nextButton.setTitle(errorMessage, forState: UIControlState.Normal)
    }
	
	func getUsername() -> String?
	{
		print(self.userNameTextField.text)
		return self.userNameTextField.text
	}
	
	//MARK: - Actions
	
	func userDidSelectNext(sender: AnyObject)
	{
		self.presenter?.userDidSelectNext()
	}
	
	func showLoading(let loadingMessage: String)
	{
		self.nextButton.setTitle(loadingMessage, forState: UIControlState.Disabled)
	}
	
	
	//MARK: - Status Bar
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		return UIStatusBarStyle.LightContent
	}
	
}
