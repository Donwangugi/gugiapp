//
//  SignUpItem.swift
//  gugiapp
//
//  Created by Aaron Wangugi on 7/6/15.
//  Copyright © 2015 Aaron Wangugi. All rights reserved.
//

import Foundation


public struct SignUpItem {
	
	public var userName: String
	public var userID: String?
	public var userEmail: String?
	public var phoneNumber: String?
}
