//
//  SignUpWireFrame.swift
//  gugiapp
//
//  Created by Aaron Wangugi on 7/17/15.
//  Copyright © 2015 Aaron Wangugi. All rights reserved.
//

import Foundation
import UIKit

class SignUpWireFrame: SignUpWireFrameProtocol
{
	class func presentSignUpModule(inWindow window: UIWindow) {
		let userNameView: UsernameViewProtocol = UsernameView()
		let presenter: protocol<SignUpPresenterProtocol, SignUpInteractorOutputProtocol> = SignUpPreseneter()
		let interactor: SignUpInteractorInputProtocol = SignUpInteractor()
		let APIDataManager: SignUpAPIDataManagerInputProtocol = SignUpAPIDataManager()
		let wireFrame: SignUpWireFrameProtocol = SignUpWireFrame()
		
		//Connect Em Up
		userNameView.presenter = presenter
		presenter.userNameView = userNameView
		presenter.wireFrame = wireFrame
		presenter.interactor = interactor
		interactor.presenter = presenter
		interactor.APIDataManager = APIDataManager
		
		//Present
		window.rootViewController = userNameView as? UsernameView
	}
	
	func presentHome(fromView view: AnyObject, completion: ((completed: Bool) -> ())?) {
		
	}
}