//
//  Styles.swift
//  gugiapp
//
//  Created by Aaron Wangugi on 7/6/15.
//  Copyright © 2015 Aaron Wangugi. All rights reserved.
//

import Foundation
import UIKit

public struct Stylesheet
{
	static let COLOR_BLUE_LIGHT: UIColor = UIColor(red: 0.0/255, green: 132.0/255, blue: 180.0/255, alpha: 1.0)
}