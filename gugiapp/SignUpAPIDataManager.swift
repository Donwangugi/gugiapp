//
//  APIDataManager.swift
//  gugiapp
//
//  Created by Aaron Wangugi on 7/10/15.
//  Copyright © 2015 Aaron Wangugi. All rights reserved.
//

import Foundation
import Alamofire

class SignUpAPIDataManager: SignUpAPIDataManagerInputProtocol {
	
	init() {}
	
	func checkUserName(completion: (error: String?) -> (), username: String?) {
		if username != nil {
			let url = URLS.BASE_URL + "/" + username!
			Alamofire.request(.GET, url)
				.responseJSON { (req, res, json, error) in
					if let err = error {
						NSLog("Error: \(err)")
                        completion(error: err.description)
					}
					else {
						NSLog("Success contacting server: \(url)")
                        if let response = res {
                            if response.statusCode != 200 && response.statusCode != 404 {
                                let error = "Error, incorrect status code: \(response.statusCode)"
                                NSLog(error)
                                completion(error: error)
                            }
                            else if response.statusCode == 200 {
                                let error = "Error, username exists"
                                NSLog(error)
                                completion(error: error)
                            }
                            else {
                                let statusString = "Success, username is free"
                                NSLog(statusString)
                                completion(error: nil)
                            }
                        }
					}
			}
		}
	}
	
	func signUp(completion: (error: String?, signupItem: SignUpItem?) -> ()) {
	}
	
}