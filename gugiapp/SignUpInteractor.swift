//
//  SignUpInteractor.swift
//  gugiapp
//
//  Created by Aaron Wangugi on 7/10/15.
//  Copyright © 2015 Aaron Wangugi. All rights reserved.
//

import Foundation

class SignUpInteractor: SignUpInteractorInputProtocol
{
	weak var presenter: SignUpInteractorOutputProtocol?
	var APIDataManager: SignUpAPIDataManagerInputProtocol?
	var localDatamanager: SignUpLocalDataManagerInputProtocol?
	
	init() {}
	
	
	// MARK: - TwitterLoginInteractorInputProtocol
	
	func signUp(completion: (error: String?) -> (), signUpItem: SignUpItem?)
	{
		self.APIDataManager?.signUp({ [weak self] (error: String?, credentials: SignUpItem?) -> () in
			if (credentials != nil) {
				self?.localDatamanager?.persistUserCredentials(credentials: credentials!)
				self?.localDatamanager?.setupLocalStorage()
				completion(error: nil)
			}
			else {
				completion(error: error)
			}
			})
	}
	
	func checkUserName(completion: (error: String?) -> (), username: String?) {
		if let name = username {
			self.APIDataManager?.checkUserName({(error: String?) -> () in
				if (error != nil) {
					completion(error: error)
				}
				else {
					completion(error: nil)
				}
				}, username: name)
		} else {
			completion(error: "No username\n")
		}
	}
		
}