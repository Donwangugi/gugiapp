//
//  SignUpProtocols.swift
//  gugiapp
//
//  Created by Aaron Wangugi on 7/6/15.
//  Copyright © 2015 Aaron Wangugi. All rights reserved.
//

import Foundation
import UIKit

protocol SignUpViewProtocol: class
{
	var presenter: SignUpPresenterProtocol? { get set }
	
	//PRESENTER -> VIEW
	func setSignUpTitle(let title: String)
	func setLogo(let image: UIImage)
	func showError(let errorMessage: String)
	func showLoading()
	func hideLoading()
	
	//View -> PRESENTER
}

protocol UsernameViewProtocol: class
{
	var presenter: SignUpPresenterProtocol? { get set }
	
	//PRESENTER -> VIEW
	func setNextButtonTitle(let title: String)
	func setLogo(let image: UIImage)
	func showError(let errorMessage: String)
	func showLoading(let loadingMessage: String)
	
	func getUsername() -> String?
}

protocol SignUpWireFrameProtocol: class
{
	static func presentSignUpModule(inWindow window: UIWindow)
	
	//PRESENTER -> WIREFRAME
	func presentHome(fromView view: AnyObject, completion: ((completed: Bool) -> ())?)
}

protocol SignUpPresenterProtocol: class
{
	var view: SignUpViewProtocol? { get set }
	var userNameView: UsernameViewProtocol? { get set }
	var interactor: SignUpInteractorInputProtocol? { get set }
	var wireFrame: SignUpWireFrameProtocol? { get set }
	
	/**
	* Add here your methods for communication VIEW -> PRESENTER
	*/
	func viewDidLoad()
	func userDidSelectSignUp()
	
	func userDidSelectNext()
	func userNameViewDidLoad()

}

protocol SignUpInteractorOutputProtocol: class
{
	/**
	* Add here your methods for communication INTERACTOR -> PRESENTER
	*/
}

protocol SignUpInteractorInputProtocol: class
{
	var presenter: SignUpInteractorOutputProtocol? { get set }
	var APIDataManager: SignUpAPIDataManagerInputProtocol? { get set }
	var localDatamanager: SignUpLocalDataManagerInputProtocol? { get set }
	/**
	* Add here your methods for communication PRESENTER -> INTERACTOR
	*/
	
	func signUp(completion: (error: String?) -> (), signUpItem: SignUpItem?)
	func checkUserName(completion: (error: String?) -> (), username: String?)
}

protocol SignUpDataManagerInputProtocol: class
{
	/**
	* Add here your methods for communication INTERACTOR -> DATAMANAGER
	*/
}

protocol SignUpAPIDataManagerInputProtocol: class
{
	/**
	* Add here your methods for communication INTERACTOR -> APIDATAMANAGER
	*/
	func signUp(completion: (error: String?, signupItem: SignUpItem?) -> ())
	func checkUserName(completion: (error: String?) -> (), username: String?)
}

protocol SignUpLocalDataManagerInputProtocol: class
{
	/**
	* Add here your methods for communication INTERACTOR -> APIDATAMANAGER
	*/
	func persistUserCredentials(credentials credentials: SignUpItem)
	func setupLocalStorage()
}
